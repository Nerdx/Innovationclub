from django.urls import path, include
from rest_framework import routers
from . import views
# from .views import MemberViewSet 

# router=routers.DefaultRouter()
# router.register(r'members', views.MemberViewSet, "members")

urlpatterns = [
    # path('', include(router.urls)),
    path('', views.members, name='members'),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('minutes/', views.minutes, name='minutes')

]



