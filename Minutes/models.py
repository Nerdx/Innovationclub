from django.db import models

class Position(models.Model):
	position_name=models.CharField(max_length=30, null=True, unique=True)
	def __str__(self):
		return self.position_name


class Members(models.Model):
	FirstName=models.CharField(max_length=20)
	LastName=models.CharField(max_length=20)
	Year=models.IntegerField()
	phone_number=models.CharField(primary_key=True,max_length=12)
	position=models.ForeignKey(Position, on_delete=models.CASCADE)

	# def __str__(self):
	# 	return self.LastName,
class Agenda(models.Model):
	id=models.AutoField(primary_key=True)
	agenda=models.CharField(max_length=255)
	def __str__(self):
		return self.agenda

class Minutes(models.Model):
	id=models.AutoField(primary_key=True)
	min_title=models.ForeignKey(Agenda, on_delete=models.CASCADE)
	minutes_body=models.TextField()


	
