from django.contrib import admin
from .models import Members,Position,Agenda,Minutes

admin.site.register(Members)
admin.site.register(Position)
admin.site.register(Agenda)
admin.site.register(Minutes)