from rest_framework import serializers
from .models import Members

class MemberSerializer(serializers.ModelSerializer):
	class Meta:
		model=Members
		fields=('FirstName', 'LastName', 'position','Year', 'phone_number')
		