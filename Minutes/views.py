from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .serializers import MemberSerializer
from .models import Members

@api_view(['POST','GET'])
def members(request):

	if request.method == 'GET':
		members = Members.objects.all()
		serializer = MemberSerializer(members, many=True)
		return Response(serializer.data)

	if request.method == 'POST':
		serializer = MemberSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



